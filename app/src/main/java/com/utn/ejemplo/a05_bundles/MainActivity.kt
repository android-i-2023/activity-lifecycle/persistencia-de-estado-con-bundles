package com.utn.ejemplo.a05_bundles

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity(), View.OnClickListener {

    /**
     * Vimos que companion object es un objeto que "acompaña" a una clase.
     * Es un objeto único, accesible por todas las instancias de la clase.
     * Colocamos aquí nuestras constantes
     */
    companion object {
        private const val ETIQUETA_LOG = "Curso"
        private const val CLAVE_MES_ACTUAL = "mesActual"
    }

    private var mesActual = 0

    /**
     * lateinit indica a Kotlin que acepte que una propiedad no esté inicializada.
     * Es nuestra responsabilidad garantizar que cuando esa propiedad necesite ser accedida,
     * ya esté efectivamente inicializada.
     * No hacerlo va a producir una excepción ni bien tratemos de acceder una propiedad sin inicializar,
     * de manera similar a cuando una variable nulleable que vale null es accedida usando `!!`
     */
    private lateinit var mes: TextView

    private lateinit var meses: Array<String>


    // savedInstanceState es el estado de la encarnación anterior de esta Activity (la _instancia_ anterior)
    override fun onCreate(savedInstanceState: Bundle?) {
        /*
            Log.d muestra un mensaje en Logcat, el flujo de logs del dispositivo Android, que se puede
            ver en la ventana inferior que dice Logcat. Se puede considerar el equivalente de println()
            para Android (println también funciona, pero no permite etiquetar los mensajes como Log.d.

            Los mensajes de Logcat tiene distintas prioridades:
            Log.v: verborrágico
            Log.d: análisis de errores
            Log.i: información
            Log.w: advertencia
            Log.e: errores
         */
        Log.d(ETIQUETA_LOG, "MainActivity.onCreate()")

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Si savedInstanceState es null, esta MainActivity no es una "reencarnación" de otra que fue destruida antes,
        // sino que es la primera de la aplicación.
        if (savedInstanceState != null) {
            mesActual = savedInstanceState.getInt(CLAVE_MES_ACTUAL) // devuelve el entero bajo esta clave, o cero si no existe
        }

        meses = resources.getStringArray(R.array.meses_del_año)

        mes = findViewById(R.id.mes)
        val anterior: Button = findViewById(R.id.anterior)
        val siguiente: Button = findViewById(R.id.siguiente)

        // La ventaja de que la Activity ya implemente View.OnClickListener es que nos ahorramos crear
        // objetos con esa interfaz y especificos de para procesar el click de un botón.
        // En este caso hay dos botones, pero si hubiera muchos esto sería más ventajoso.
        anterior.setOnClickListener(this)
        siguiente.setOnClickListener(this)
        actualizarMes()
    }

    /*
        Este método se ejecuta justo antes del momento en que una Activity va a ser destruida.
        Esto puede ocurrir cuando el usuario rota la pantalla, cuando cambia el idioma del teléfono,
        o el tamaño de la letra, cuando se pasa al modo oscuro o al claro, etc.
     */
    override fun onDestroy() {
        super.onDestroy()
        Log.d(ETIQUETA_LOG, "MainActivity.onCreate()")
    }

    /*
        Este método se ejecuta cuando la Activity está por ser destruída y debe guardar su estado en un Bundle,
        para poder pasárselo a la próxima encarnación de la Activity llegado el momento.
        outState es un Bundle al que escribimos para guardar el estado.
     */
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(CLAVE_MES_ACTUAL, mesActual)
    }


    // Redefinición de View.OnClickListener.onClick()
    override fun onClick(v: View?) {
        if (v == null) {
            return
        }

        // Según cuál botón fue clickeado, hacer algo distinto
        when (v.id) {
            R.id.anterior -> retroceder()
            R.id.siguiente -> avanzar()
        }
    }

    private fun retroceder() {
        mesActual = mesActual - 1
        if (mesActual < 0) {
            mesActual += meses.size
        }
        actualizarMes()
    }

    private fun avanzar() {
        mesActual = mesActual + 1
        if (mesActual == meses.size) {
            mesActual = 0
        }
        actualizarMes()
    }

    private fun actualizarMes() {
        mes.text = meses[mesActual]
    }
}
